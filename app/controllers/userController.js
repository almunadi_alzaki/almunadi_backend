import Responder from '../../lib/expressResponder';
import User from '../models/user'

export default class UserController {
  static page(req, res) {
      User.find({})
    .then(all_user => Responder.success(res, {
        success: true,
        data: all_user,
        message: " all User list  Successfully!!"
    }))
    .catch(errorOnDBOp => Responder.operationFailed(res, errorOnDBOp));
  }

  static count(req, res) {
    //
    User.find({})
    .then(all_user => Responder.success(res, {
        success: true,
        data: all_user,
        message: " all User list  Successfully!!"
    }))
    .catch(errorOnDBOp => Responder.operationFailed(res, errorOnDBOp));

  }

  static show(req, res) {
    
  }

  static create(req, res) {
    User.create(req.body)
    .then(add => Responder.success(res, {
        success: true,
        data: add,
        message: "User Add Successfully!!"
    }))
    .catch(errorOnDBOp => Responder.operationFailed(res, errorOnDBOp));
  }

  static update(req, res) {
    console.log(req.body);

    let test = req.body;
    User.findOneAndUpdate({ _id: req.params.id }, { $set: test }, { new: true })
      .then(updateStatus => Responder.success(res, {
        data: updateStatus,
        success: true,
        message: "user updated Successfully!!"
      }))
      .catch(errorOnDBOp => Responder.operationFailed(res, errorOnDBOp));

  }

  static remove(req, res) {
    User.remove({_id:req.params.id})
    .then(updateStatus => User.find({})
    .then(all_user => Responder.success(res, {
        success: true,
        data: all_user,
        message: " all User list  Successfully!!"
    }))

    )
    .catch(errorOnDBOp => Responder.operationFailed(res, errorOnDBOp));
  }
}
