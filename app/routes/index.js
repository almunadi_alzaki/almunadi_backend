import initPlanRoutes from './planRoutes';
import initUserRoutes from './userRoutes';

const initRoutes = (app) => {
  app.use(`/foos`, initPlanRoutes());
  app.use(`/user`,initUserRoutes());
};

export default initRoutes;
