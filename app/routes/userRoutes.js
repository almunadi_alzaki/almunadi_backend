import express from 'express';
import UserController from '../controllers/userController';

const initUserRoutes = () => {
  const userRoutes = express.Router();

  userRoutes.get('/', UserController.page);
  userRoutes.get('/:planId', UserController.show);
  userRoutes.post('/', UserController.create);
  userRoutes.put('/update/:id', UserController.update);
  userRoutes.delete('/:id', UserController.remove);

  return userRoutes;
};

export default initUserRoutes;
