import mongoose from 'mongoose';
const Schema = mongoose.Schema;



const UserSchema = new Schema({
 stud_name : {
     type:String,
     required:true
 },
 mob_no : {
     type :String,
     required: true
 }

  
});

export default mongoose.model('User', UserSchema);
